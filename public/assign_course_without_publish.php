<?php
/**
 * Created by IntelliJ IDEA.
 * User: fmarcelo
 * Date: 2/20/13
 * Time: 3:14 PM
 * To change this template use File | Settings | File Templates.
 */

set_include_path(implode(PATH_SEPARATOR, array(
  realpath('../vendor'),
  get_include_path(),
)));

require_once 'UUID/UUID.php';
require_once 'Zend/Loader.php';
Zend_Loader::loadClass('Zend_Http_Client');

$APPKEY = 'AA115507-0182-4E71-8F74-D9843B85E5A4';
$SecretKey = 'amrgUwecO7g6xrMXN';
$ClientVersion = '1.0';
$ClientId = '3CF0FF3C-7D85-4F10-92C9-A41046694832'; // at least 18 symbols original system root account
$ClientId = 'f3d7d637-c396-4cf7-e8ab-0c9f8291abc1';//account CSB_LMS_Application
$GlobalUserId  = 'deb0a18f-78aa-4382-9a6e-4e2605733dbd';//system root account
$GlobalUserId  = '66ea28f5-7597-4883-a3c0-a15e0009d5f2';//account of fmarceloo@csod.com
//$GlobalUserId  = '44d76ec9-2672-43ef-a290-a15e002de3e3';
$ApplicationId = '7cec460a-9ea6-4024-b94a-a15e0007c16e';//CSB_LMS_Application main account
$GlobalUserSession = '';

function GetSignature($method=null,$url,$contentLength ,$date)
{
  global $APPKEY,$SecretKey,$ClientVersion,$ClientId,$GlobalUserId,$ApplicationId,$GlobalUserSession;

  $sign = $method ."\n".strtolower($url)."\n" . $APPKEY."\n".$date."\n".$contentLength;
  if( $method =='POST' ){
   // echo $sign;die;
  }
  $hmac = base64_encode(hash_hmac("sha1",utf8_encode($sign),utf8_encode($SecretKey),true));

  return strtolower(urlencode($hmac));
}

function GetAuthHeader($signature=null, $date=null)
{
  global $APPKEY,$ClientId,$ClientVersion;

  return 'cyberu ' . $APPKEY . '::' . $date .  '::' . strtolower($signature) . '::' . $ClientId .'::'.$ClientVersion;
}

function unserialize_xml($input, $callback = null, $recurse = false)
{
  $data = ((!$recurse) && is_string($input))? simplexml_load_string($input): $input;
  if ($data instanceof SimpleXMLElement) $data = (array) $data;
  if (is_array($data)) foreach ($data as &$item) $item = unserialize_xml($item, $callback, true);
  return (!is_array($data) && is_callable($callback))? call_user_func($callback, $data): $data;
}

/**
 * Generate Authorization Session Key
 *
 */

$method = 'GET';
$date   = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$url    = 'http://cornerstone.cyberu-api.com/service.svc/session/' .$GlobalUserId;
$signature = GetSignature($method, $url, -1, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
    'maxredirects' => 0,
    'timeout' => 30,
    'useragent' => 'CSOD_Sandbox',
    'keepalive' => true
  )
);
$client->setHeaders('Authorization',$authorization);
$client->setMethod(Zend_Http_Client::GET);
$response = $client->request();

$ctype = $response->getHeader('Content-type');
if (is_array($ctype)) $ctype = $ctype[0];

$token = $response->getBody();
if ($ctype == 'text/html' || $ctype == 'text/xml') {
  $token = htmlentities($token);
}

$UserId = '44d76ec9-2672-43ef-a290-a15e002de3e3';
#$CourseId = '4fae6684-4c30-473a-92bb-a166005f1639';
$CourseId = 'cb36ab69-a0d4-45e7-804e-a15e017056cf';

$url           = 'http://cornerstone.cyberu-api.com/service.svc/user/'.$UserId.'/course/'.$CourseId;


$CourseId = '995795b1-e0e2-4a09-b2c1-a1790063cd68';

/*
   
[4:42:49 PM] Frank Marcelo: organization E5920FFD-2377-4132-8A30-A17700576B39
[4:45:20 PM] Frank Marcelo: learning resourceId: 995795b1-e0e2-4a09-b2c1-a1790063cd68
*/

$user_account_request = array(
    array('Id'=>'bcd87ebc-0258-497a-b421-a17e0182da2e'),
    array('Id'=>'63f5b8d0-0c8b-41f5-9c04-a17100f0c276')
);
$user_accounts = array('user'=>$user_account_request);

$body  = http_build_query($user_accounts);

//echo urldecode($body);die;

//$url           = 'http://cornerstone.cyberu-api.com/service.svc/groups/'.$ApplicationId.'/users?'.urldecode($body);


$url = 'http://cornerstone.cyberu-api.com/service.svc/course/'.$CourseId.'/users?'.urldecode($body);
$method        = 'POST';
$date          = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$signature     = GetSignature($method, $url, 0, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
    'maxredirects' => 0,
    'timeout' => 30,
    'useragent' => 'CSOD_Sandbox',
    'keepalive' => true
  )
);

$client->setHeaders(array(
    'Authorization' => $authorization,
    'x-cyberu-auth' => $token,
  )
);
$client->setMethod(Zend_Http_Client::POST);
$response = $client->request();

$domRQ = new DOMDocument();
$domRQ->loadXML($response->getBody());
$domRQ->formatOutput = true;
header("Content-type: text/xml");
echo $domRQ->saveXML();
//echo print_r($client->getLastRequest(),true);
//echo print_r($client->getLastResponse(),true);