<?php

set_include_path(implode(PATH_SEPARATOR, array(
  realpath('../vendor'),
  get_include_path(),
)));


//echo get_include_path();
require_once 'Zend/Loader.php';
require_once 'UUID/UUID.php';
Zend_Loader::loadClass('Zend_Http_Client');

$APPKEY = 'AA115507-0182-4E71-8F74-D9843B85E5A4';
$SecretKey = 'amrgUwecO7g6xrMXN';
$ClientVersion = '1.0';
$ClientId = '3CF0FF3C-7D85-4F10-92C9-A41046694832'; // at least 18 symbols original system root account
$ClientId = 'f3d7d637-c396-4cf7-e8ab-0c9f8291abc1';//account CSB_LMS_Application
$GlobalUserId  = 'deb0a18f-78aa-4382-9a6e-4e2605733dbd';//system root account
$GlobalUserId  = '66ea28f5-7597-4883-a3c0-a15e0009d5f2';//account of fmarceloo@csod.com
//$GlobalUserId  = '44d76ec10-2672-43ef-a290-a15e002de3e3';
$ApplicationId = '7cec460a-9ea6-4024-b94a-a15e0007c16e';//CSB_LMS_Application main account
$GlobalUserSession = '';

function GetSignature($method=null,$url,$contentLength ,$date)
{
    global $APPKEY,$SecretKey,$ClientVersion,$ClientId,$GlobalUserId,$ApplicationId,$GlobalUserSession;

    $sign = $method ."\n".strtolower($url)."\n" . $APPKEY."\n".$date."\n".$contentLength;
    $hmac = base64_encode(hash_hmac("sha1",utf8_encode($sign),utf8_encode($SecretKey),true));

    return strtolower(urlencode($hmac));
}

function GetAuthHeader($signature=null, $date=null)
{
    global $APPKEY,$ClientId,$ClientVersion;

    return 'cyberu ' . $APPKEY . '::' . $date .  '::' . strtolower($signature) . '::' . $ClientId .'::'.$ClientVersion;
}

function unserialize_xml($input, $callback = null, $recurse = false)
{
    $data = ((!$recurse) && is_string($input))? simplexml_load_string($input): $input;
    if ($data instanceof SimpleXMLElement) $data = (array) $data;
    if (is_array($data)) foreach ($data as &$item) $item = unserialize_xml($item, $callback, true);
    return (!is_array($data) && is_callable($callback))? call_user_func($callback, $data): $data;
}

$method = 'GET';
$date   = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$url    = 'http://cornerstone.cyberu-api.com/service.svc/session/' .$GlobalUserId;
$signature = GetSignature($method, $url, -1, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
      'maxredirects' => 0,
      'timeout' => 30,
      'useragent' => 'CSOD_Sandbox',
      'keepalive' => true
  )
);
$client->setHeaders('Authorization',$authorization);
$client->setMethod(Zend_Http_Client::GET);
$response = $client->request();

$ctype = $response->getHeader('Content-type');
if (is_array($ctype)) $ctype = $ctype[0];

$token = $response->getBody();
if ($ctype == 'text/html' || $ctype == 'text/xml') {
    $token = htmlentities($token);
}

$uuid = UUID::generate(UUID::UUID_RANDOM, UUID::FMT_STRING);

//CREATE APPLICATION
$title = 'Sonar6';
$callback = '';
$data = array(
    'title' => $title ,
    'callback' => $callback,
    'ref' => $uuid
);

//$token = 'QenUKS8bJ2Eg67u/kI2+HwGtr1ckJUDfcmBWaLdDNefb0JmMBp/27Wn1B8THIHLsZNgqhDrbKHp3ZDJ4Tnw64d0pWimgZ/PJ9DzSbqo8/4DbfTXWuOr57bAcahK/csrD8lUBwcrfgO48EYjryBKH6iV5tiDI3KaWqQIEjwsbt8Kcw3/AcV9wR01gfeJ4/fR2qBthdEHeIZK6Ekq+uW+6aCQK1s9/F09jOGNPKuxu9zaIZXMD85bxmbIqe9UBkHaSKIefc25v7cYp2PiZIPPFLfvpunVqWxwfJQb29DsgGrUrNkC9LEo1aRIyUvNPlNTgi7lknnq+1ecfcshF3KoXVJX+IY5U0sUgHJDrdwcuVOjDhKvXLqFjRVfFjqIBWfyH7aitzOGExtkjyQzLyi/t6UAzm+GFalOt9M590VZ4hr8QITbfJugs3AN+vuPgL2BID2nP41crdKvnVc3z4kIkWqz7+a+DdZF/IQ5jUlAp5xnLgdCytFWVUyS/5TPdkm8XjpeOyQIdlCygHzthFqN/IpoJ6e9rsvni1LBIJ7lK3DdTEptaS5xQvNth549Z+ubnyZ2P9iujWG+/ZRQCstvb46Mc+Agv0izfd9SlZLw10r1kVGBATH4IpaVe3JMR3bX7z5UVhcbxJ5/45UKAfa+B3tuMjy+Wj5+KjVzL6CZKfiu+ReNpx6eBUYfYha9yj/xZgAWQ0wTcx9FcnM6cyz816XDJ9lvWm5zm6mD9K8i0OsGhgwJsqNY+EgRVzGj3sltvjl1KBsRf+0M/G9iNuIHT8ZsUG0rRg8cXWeZLx4E3KvlzXMnm2FOfpYM5alyk+psEeyVWYCfCq4Jo5VPpl4YqQ06szmetvZfXMIlYVWtqpKXkduU2XgRrIOnQkIl/Ry98Z/cFY3f27XHcc7LK6iWKDc/4qeTtIUDIghiMxUlBeyvoRM7GDwBiU9jwQVkHBlEsm8MKmPKnEkCTVxO6rnP0YCsiddiaoKPU7iI8ykJZHbBJs/bH4z/I7QKvB0+F360BmqyGUkz4mhQdcfSM0c5GV2rerS6moq0hSqFSHY7ZGdCLHI8C87GpoeTe2hz+Fkg8z+Lm9nD7nEqNWSIktjFaeU/s70u2eJ4Sm4ONz33oJWBrKiL6YDNHoVOESjEl92qqCFuRBqeLlqptkD/cFH7rsf9pWXcvpD1K5H0XdF5OTVQZPRAIwxiJwmlrPeQ83CNLC0l9EaqygcXGezVXr+YQeIHSSQWb8IKKYD/bECBvi+xTKFFikuz33Lkj+50q242fwj7alSxJ3t2Ne169JNT5xA==';

 /*
POST
http://cornerstone.cyberu-api.com/service.svc/groups?title=csb_organisation&ref=28c063e4-a5fc-4b8a-d4d0-741faa317057&callbackurl=
AA115507-0182-4E71-8F74-D9843B85E5A4

0*/
$url           = 'http://cornerstone.cyberu-api.com/service.svc/groups?'.http_build_query($data);
//$url = 'http://cornerstone.cyberu-api.com/service.svc/groups?title=csb_organisation&ref=28c063e4-a5fc-4b8a-d4d0-741faa317057&callbackurl=';
$method        = 'POST';
$date          = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$signature     = GetSignature($method, $url, 0, $date);
$authorization = GetAuthHeader($signature,$date);
$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
      'maxredirects' => 0,
      'timeout' => 30,
      'useragent' => 'CSOD_Sandbox',
      'keepalive' => true
  )
);

$client->setHeaders(array(
      'Authorization' => $authorization,
      'x-cyberu-auth' => $token,
  )
);
$client->setMethod(Zend_Http_Client::POST);

$response = $client->request();

$domRQ = new DOMDocument();
$domRQ->loadXML($response->getBody());
$domRQ->formatOutput = true;
header("Content-type: text/xml");
echo $domRQ->saveXML();
//$applicationInfo = unserialize_xml($domRQ->saveXML());//conver xml nodes to array
//$ApplicationId = trim($applicationInfo['Id']);//get Application Id

/* Sample XML
<Ou xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
<Active>false</Active>
<Application>AA115507-0182-4E71-8F74-D9843B85E5A4</Application>
<CallbackUrl>www.sonar6.com</CallbackUrl>
<ClientId>5649ca4b-36e1-4bea-caa0-8f12c26a9463</ClientId>
<Id>24c76cf5-b0fa-45d3-a445-a1640043b887</Id>
<Properties/>
<RecordType>ou</RecordType>
<RequestLink i:nil="true"/>
<Timestamp>2013-02-13T04:06:33.867477Z</Timestamp>
<Title>CSB_LMS_Application_Test_By_Member_Account</Title>
<VisibleToOus xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
</Ou>
*/
//echo print_r($client->getLastRequest(),true);
//echo print_r($client->getLastResponse(),true);
