<?php

set_include_path(implode(PATH_SEPARATOR, array(
  realpath('../vendor'),
  get_include_path(),
)));


//echo get_include_path();
require_once 'Zend/Loader.php';
require_once 'UUID/UUID.php';
Zend_Loader::loadClass('Zend_Http_Client');

$APPKEY = 'AA115507-0182-4E71-8F74-D9843B85E5A4';
$SecretKey = 'amrgUwecO7g6xrMXN';
$ClientVersion = '1.0';
$ClientId = '3CF0FF3C-7D85-4F10-92C9-A41046694832'; // at least 18 symbols original system root account
$ClientId = 'f3d7d637-c396-4cf7-e8ab-0c9f8291abc1';//account CSB_LMS_Application
$GlobalUserId  = 'deb0a18f-78aa-4382-9a6e-4e2605733dbd';//system root account
$GlobalUserId  = '66ea28f5-7597-4883-a3c0-a15e0009d5f2';//account of fmarceloo@csod.com
//$GlobalUserId  = '44d76ec9-2672-43ef-a290-a15e002de3e3';
$ApplicationId = '7cec460a-9ea6-4024-b94a-a15e0007c16e';//CSB_LMS_Application main account
$GlobalUserSession = '';

function GetSignature($method=null,$url,$contentLength ,$date)
{
    global $APPKEY,$SecretKey,$ClientVersion,$ClientId,$GlobalUserId,$ApplicationId,$GlobalUserSession;
    $sign = $method ."\n".strtolower($url)."\n" . $APPKEY."\n".$date."\n".$contentLength;
    $hmac = base64_encode(hash_hmac("sha1",utf8_encode($sign),utf8_encode($SecretKey),true));

    return strtolower(urlencode($hmac));
}

function GetAuthHeader($signature=null, $date=null)
{
    global $APPKEY,$ClientId,$ClientVersion;

    return 'cyberu ' . $APPKEY . '::' . $date .  '::' . strtolower($signature) . '::' . $ClientId .'::'.$ClientVersion;
}

function unserialize_xml($input, $callback = null, $recurse = false)
{
    $data = ((!$recurse) && is_string($input))? simplexml_load_string($input): $input;
    if ($data instanceof SimpleXMLElement) $data = (array) $data;
    if (is_array($data)) foreach ($data as &$item) $item = unserialize_xml($item, $callback, true);
    return (!is_array($data) && is_callable($callback))? call_user_func($callback, $data): $data;
}

/**
 * Generate Authorization Session Key
 *
 */

$method = 'GET';
$date   = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$url    = 'http://cornerstone.cyberu-api.com/service.svc/session/' .$GlobalUserId;
$signature = GetSignature($method, $url, -1, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
      'maxredirects' => 0,
      'timeout' => 30,
      'useragent' => 'CSOD_Sandbox',
      'keepalive' => true
  )
);
$client->setHeaders('Authorization',$authorization);
$client->setMethod(Zend_Http_Client::GET);
$response = $client->request();

$ctype = $response->getHeader('Content-type');
if (is_array($ctype)) $ctype = $ctype[0];

$token = $response->getBody();
if ($ctype == 'text/html' || $ctype == 'text/xml') {
    $token = htmlentities($token);
}

//Array ( [ref] =>  [email] => frankitoy@orcon.co.nz [name] => Frank Marcelo [callback] => )

$users =
  array(
    'ref'      => UUID::generate(UUID::UUID_RANDOM, UUID::FMT_STRING),
    'email'    => 'demo.dgeorge@sonar6.com',
    'name'     => 'Daniel George',
    'callback' => null
  );

$user = array('user'=>array($users));

$body          = http_build_query($user);
$url           = 'http://cornerstone.cyberu-api.com/service.svc/users/r';
$method        = 'POST';
$signature     = GetSignature($method, $url, strlen($body), $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
      'maxredirects' => 0,
      'timeout' => 30,
      'useragent' => 'CSB_Small_Business',
      'keepalive' => true
  )
);

$client->setHeaders(array(
      'Authorization' => $authorization,
      'x-cyberu-auth' => $token,
  )
);
$client->setParameterPost($user);
$client->setMethod(Zend_Http_Client::POST);
$response = $client->request();

header("Content-type: text/xml");
$domRQ = new DOMDocument();
$domRQ->loadXML($response->getBody());
$domRQ->formatOutput = true;
echo $domRQ->saveXML();

//echo print_r($client->getLastRequest(),true);
//echo print_r($client->getLastResponse(),true);
die;
/*
 * Traverse through the response xml nodes. Get the UserId value under Role parent element
 *
<?xml version="1.0"?>
<Users xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
  <User>
    <Active>true</Active>
    <ApplicationId>02e16d0d-d906-408e-91d2-edd98f927b6f</ApplicationId>
    <ApplicationKey>AA115507-0182-4E71-8F74-D9843B85E5A4</ApplicationKey>
    <Callback>www.cwc2019.com</Callback>
    <ClearEmail>doejohn@gmail.com</ClearEmail>
    <ClientId i:nil="true"/>
    <Email>doejohn@gmail.com</Email>
    <Id>46ce0a32-c8ad-4643-a8d7-a15d0178f2bd</Id>
    <Name>Doe John</Name>
    <Password/>
    <PortalId i:nil="true"/>
    <RecordType>user</RecordType>
    <RefId>3CF0FF3C-7D85-4F10-92C9-A410466948322</RefId>
    <Roles>
      <Role>
        <ApplicationId>02e16d0d-d906-408e-91d2-edd98f927b6f</ApplicationId>
        <Id>3ea719d6-4058-4520-ad71-a15d0178f2bd</Id>
        <OuName>Cornerstone</OuName>
        <RecordType>ouuser</RecordType>
        <Role>member</Role>
        <Timestamp>2013-02-06T22:52:25.5829371Z</Timestamp>
        <UserId>46ce0a32-c8ad-4643-a8d7-a15d0178f2bd</UserId>
      </Role>
    </Roles>
    <SalesforcePassword i:nil="true"/>
    <Timestamp>2013-02-06T22:52:25.5829371Z</Timestamp>
  </User>
  <User>
    <Active>true</Active>
    <ApplicationId>02e16d0d-d906-408e-91d2-edd98f927b6f</ApplicationId>
    <ApplicationKey>AA115507-0182-4E71-8F74-D9843B85E5A4</ApplicationKey>
    <Callback>www.csodabcccccccccsdsassdasdc.com</Callback>
    <ClearEmail>frankitoy@xyzzzzzzzsdsdsd.com</ClearEmail>
    <ClientId i:nil="true"/>
    <Email>frankitoy@xyzzzzzzzsdsdsd.com</Email>
    <Id>d7ec1faa-8bed-48f7-8b64-a15c00310cc9</Id>
    <Name>Frank Abcccccccccsdsdsdsd</Name>
    <Password/>
    <PortalId i:nil="true"/>
    <RecordType>user</RecordType>
    <RefId>3CF0FF3C-7D85-4F10-92C9-A4104669483217</RefId>
    <Roles>
      <Role>
        <ApplicationId>02e16d0d-d906-408e-91d2-edd98f927b6f</ApplicationId>
        <Id>2b23f541-b617-431d-b8c3-a15c00310cc9</Id>
        <OuName>Cornerstone</OuName>
        <RecordType>ouuser</RecordType>
        <Role>member</Role>
        <Timestamp>2013-02-05T02:58:35</Timestamp>
        <UserId>d7ec1faa-8bed-48f7-8b64-a15c00310cc9</UserId>
      </Role>
      <Role>
        <ApplicationId>c6bb754a-cb62-40b7-aa94-a15d0176b7b4</ApplicationId>
        <Id>2fb19f57-4f49-412a-9b20-a15d0176b9a9</Id>
        <OuName>CSOD_Sandbox</OuName>
        <RecordType>ouuser</RecordType>
        <Role>admin</Role>
        <Timestamp>2013-02-06T22:44:19</Timestamp>
        <UserId>d7ec1faa-8bed-48f7-8b64-a15c00310cc9</UserId>
      </Role>
    </Roles>
    <SalesforcePassword i:nil="true"/>
    <Timestamp>2013-02-05T02:58:35</Timestamp>
  </User>
</Users>

$domRQ = new DOMDocument();
$domRQ->loadXML($response->getBody());
$domRQ->formatOutput = true;
$usersInfo = unserialize_xml($domRQ->saveXML());//conver xml nodes to array
$accounts = array();
foreach ($usersInfo['User'] as $UserKey => $User) {
    if ( !is_numeric($UserKey) ) {

        //single array object
        SimpleXMLElement Object(
          [User] => SimpleXMLElement Object(
                [Active] => true
                [ApplicationId] => 02e16d0d-d906-408e-91d2-edd98f927b6f
                ........
          )
        )
        $UserId = trim($usersInfo['User']['Roles']['Role']['UserId']);
        $accounts[] = $UserId;
    } else {
        //multiple array object
        SimpleXMLElement Object
        (
            [User] => Array(
                [0] => SimpleXMLElement Object(
                    [Active] => true
                    [ApplicationId] => 02e16d0d-d906-408e-91d2-edd98f927b6f
                    [ApplicationKey] => AA115507-0182-4E71-8F74-D9843B85E5A4
                    ....
                )
                [1] => SimpleXMLElement Object(
                    [Active] => true
                    [ApplicationId] => 02e16d0d-d906-408e-91d2-edd98f927b6f
                    [ApplicationKey] => AA115507-0182-4E71-8F74-D9843B85E5A4
                )

        if ( isset($User['Roles']['Role']['UserId']) ) {
            $UserId = trim($User['Roles']['Role']['UserId']);
            $accounts[] = $UserId;
        } else {
            foreach ($User['Roles']['Role'] as $roleInfo) {
                $UserId = $roleInfo['UserId'];
                $accounts[] = $UserId;
            }
        }
    }

}

*/
