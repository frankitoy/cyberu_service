<?php

set_include_path(implode(PATH_SEPARATOR, array(
  realpath('../vendor'),
  get_include_path(),
)));


//echo get_include_path();
require_once 'Zend/Loader.php';
require_once 'UUID/UUID.php';

Zend_Loader::loadClass('Zend_Http_Client');

$APPKEY = 'AA115507-0182-4E71-8F74-D9843B85E5A4';
$SecretKey = 'amrgUwecO7g6xrMXN';
$ClientVersion = '1.0';
$ClientId = '3CF0FF3C-7D85-4F10-92C9-A41046694832'; // at least 18 symbols original system root account
$ClientId = 'f3d7d637-c396-4cf7-e8ab-0c9f8291abc1';//account CSB_LMS_Application
$GlobalUserId  = 'deb0a18f-78aa-4382-9a6e-4e2605733dbd';//system root account
$GlobalUserId  = '66ea28f5-7597-4883-a3c0-a15e0009d5f2';//account of fmarceloo@csod.com
//$GlobalUserId  = '44d76ec9-2672-43ef-a290-a15e002de3e3';
$ApplicationId = '7cec460a-9ea6-4024-b94a-a15e0007c16e';//CSB_LMS_Application main account
$GlobalUserSession = '';

function GetSignature($method=null,$url,$contentLength ,$date)
{
    global $APPKEY,$SecretKey,$ClientVersion,$ClientId,$GlobalUserId,$ApplicationId,$GlobalUserSession;

    $sign = $method ."\n".strtolower($url)."\n" . $APPKEY."\n".$date."\n".$contentLength;
    $hmac = base64_encode(hash_hmac("sha1",utf8_encode($sign),utf8_encode($SecretKey),true));

    return strtolower(urlencode($hmac));
}

function GetAuthHeader($signature=null, $date=null)
{
    global $APPKEY,$ClientId,$ClientVersion;

    return 'cyberu ' . $APPKEY . '::' . $date .  '::' . strtolower($signature) . '::' . $ClientId .'::'.$ClientVersion;
}

function unserialize_xml($input, $callback = null, $recurse = false)
{
    $data = ((!$recurse) && is_string($input))? simplexml_load_string($input): $input;
    if ($data instanceof SimpleXMLElement) $data = (array) $data;
    if (is_array($data)) foreach ($data as &$item) $item = unserialize_xml($item, $callback, true);
    return (!is_array($data) && is_callable($callback))? call_user_func($callback, $data): $data;
}

/**
 * Generate Authorization Session Key
 *
 */

$method = 'GET';
$date   = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$url    = 'http://cornerstone.cyberu-api.com/service.svc/session/' .$GlobalUserId;
$signature = GetSignature($method, $url, -1, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
      'maxredirects' => 0,
      'timeout' => 30,
      'useragent' => 'CSOD_Sandbox',
      'keepalive' => true
  )
);
$client->setHeaders('Authorization',$authorization);
$client->setMethod(Zend_Http_Client::GET);
$response = $client->request();

$ctype = $response->getHeader('Content-type');
if (is_array($ctype)) $ctype = $ctype[0];

$token = $response->getBody();
if ($ctype == 'text/html' || $ctype == 'text/xml') {
    $token = htmlentities($token);
}

$uuid = UUID::generate(UUID::UUID_RANDOM, UUID::FMT_STRING);

/*
Id	Timestamp	Guid	Title	UploadDate	StatusId	ApplicationId
757BF5F8-9B95-49AC-AB66-A15E0170565A	2013-02-07 22:21:04.500	8627e914-46bc-4f60-8330-94a130c2e45e	Test upload	2013-02-07 22:21:04.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
648AD466-E5A6-41CC-852A-A15E01707C6A	2013-02-07 22:21:36.980	f07e907c-96f8-458c-bfb7-3a0b54e5ad43	Test upload 2	2013-02-07 22:21:36.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
BB104853-2BB1-4B0C-B2CD-A15E0170D549	2013-02-07 22:22:52.820	6cef3d31-b72c-4bad-a43c-134372d2220a	Crocodile	2013-02-07 22:22:52.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
CDDD3961-73C5-4966-80A2-A15E01893EAA	2013-02-07 23:51:45.690	0a3c455c-05e7-45b9-bc43-1d6ea4e0e2b4	Titile	2013-02-07 23:51:45.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
A1E764FA-DE97-4D6B-A2F6-A162003885F0	2013-02-11 03:25:47.680	1b604ec1-d937-4b4d-9b32-ba63f280381c	Test	2013-02-11 03:25:47.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
5B246131-CE0F-48A1-8478-A162015811CB	2013-02-11 20:52:43.120	067bcfb3-5c8b-4402-acce-9e70d3b1fc12	Sample Scorm	2013-02-11 20:52:43.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
F7F5C199-9FDB-4B3F-AF7C-A16201591042	2013-02-11 20:56:20.260	7c49c013-753c-4725-9217-790e67c857da	SCORM	2013-02-11 20:56:20.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
DD976696-002D-4441-9FFB-A16201595109	2013-02-11 20:57:15.540	4beece93-9ecc-46a2-beff-bdc36b826c19	SCORM	2013-02-11 20:57:15.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
5CAFF3D0-D3C0-43D0-88FE-A162015F3ADA	2013-02-11 21:18:47.330	abb6f3c0-b097-4309-bd6e-65ff7ca4c595	scorm lessson	2013-02-11 21:18:47.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
32861F27-14E6-4EE3-A9EB-A162015F9DD1	2013-02-11 21:20:11.780	ca722951-22e4-4d4e-b108-1584f1ca3b54	SCORMtest	2013-02-11 21:20:11.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E
70818904-4882-480D-9F2C-A1630059235A	2013-02-12 05:24:32.510	8bf11f91-60bd-4c5d-be02-ac4fdfd55bef	New Content	2013-02-12 05:24:32.000	Success	7CEC460A-9EA6-4024-B94A-A15E0007C16E

*/

$uploadId      = '757BF5F8-9B95-49AC-AB66-A15E0170565A';
$uploadId      = '995795b1-e0e2-4a09-b2c1-a1790063cd68';
//$uploadId      = 'A1E764FA-DE97-4D6B-A2F6-A162003885F0';
//$uploadId      = 'F7F5C199-9FDB-4B3F-AF7C-A16201591042';
$url           = 'http://cornerstone.cyberu-api.com/Service.svc/uploads/'.$uploadId;
$method        = 'GET';
$date          = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$signature     = GetSignature($method, $url, -1, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
    (
      'maxredirects' => 0,
      'timeout' => 30,
      'useragent' => 'CSOD_Sandbox',
      'keepalive' => true
    )
);

$client->setHeaders(array(
      'Authorization' => $authorization,
      'x-cyberu-auth' => $token,
  )
);
$client->setMethod(Zend_Http_Client::GET);
$response = $client->request();

header("Content-type: text/xml");
$domRQ = new DOMDocument();
$domRQ->loadXML($response->getBody());
$domRQ->formatOutput = true;
echo $domRQ->saveXML();
//echo print_r($client->getLastRequest(),true);
//echo print_r($client->getLastResponse(),true);
die;
