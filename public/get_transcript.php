<?php
/**
 * Created by IntelliJ IDEA.
 * User: fmarcelo
 * Date: 2/20/13
 * Time: 3:14 PM
 * To change this template use File | Settings | File Templates.
 */

set_include_path(implode(PATH_SEPARATOR, array(
  realpath('../vendor'),
  get_include_path(),
)));


//echo get_include_path();
require_once 'Zend/Loader.php';
require_once 'UUID/UUID.php';
Zend_Loader::loadClass('Zend_Http_Client');

$APPKEY = 'AA115507-0182-4E71-8F74-D9843B85E5A4';
$SecretKey = 'amrgUwecO7g6xrMXN';
$ClientVersion = '1.0';
$ClientId = '3CF0FF3C-7D85-4F10-92C9-A41046694832'; // at least 18 symbols original system root account
$ClientId = 'f3d7d637-c396-4cf7-e8ab-0c9f8291abc1';//account CSB_LMS_Application
$GlobalUserId  = 'deb0a18f-78aa-4382-9a6e-4e2605733dbd';//system root account
$GlobalUserId  = '66ea28f5-7597-4883-a3c0-a15e0009d5f2';//account of fmarceloo@csod.com
//$GlobalUserId  = '44d76ec9-2672-43ef-a290-a15e002de3e3';
$ApplicationId = '7cec460a-9ea6-4024-b94a-a15e0007c16e';//CSB_LMS_Application main account
$GlobalUserSession = '';

function GetSignature($method=null,$url,$contentLength ,$date)
{
  global $APPKEY,$SecretKey,$ClientVersion,$ClientId,$GlobalUserId,$ApplicationId,$GlobalUserSession;

  $sign = $method ."\n".strtolower($url)."\n" . $APPKEY."\n".$date."\n".$contentLength;
  $hmac = base64_encode(hash_hmac("sha1",utf8_encode($sign),utf8_encode($SecretKey),true));

  return strtolower(urlencode($hmac));
}

function GetAuthHeader($signature=null, $date=null)
{
  global $APPKEY,$ClientId,$ClientVersion;

  return 'cyberu ' . $APPKEY . '::' . $date .  '::' . strtolower($signature) . '::' . $ClientId .'::'.$ClientVersion;
}

function unserialize_xml($input, $callback = null, $recurse = false)
{
  $data = ((!$recurse) && is_string($input))? simplexml_load_string($input): $input;
  if ($data instanceof SimpleXMLElement) $data = (array) $data;
  if (is_array($data)) foreach ($data as &$item) $item = unserialize_xml($item, $callback, true);
  return (!is_array($data) && is_callable($callback))? call_user_func($callback, $data): $data;
}

/**
 * Generate Authorization Session Key
 *
 */

$method = 'GET';
$date   = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$url    = 'http://cornerstone.cyberu-api.com/service.svc/session/' .$GlobalUserId;
$signature = GetSignature($method, $url, -1, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
    'maxredirects' => 0,
    'timeout' => 30,
    'useragent' => 'CSOD_Sandbox',
    'keepalive' => true
  )
);
$client->setHeaders('Authorization',$authorization);
$client->setMethod(Zend_Http_Client::GET);
$response = $client->request();

$ctype = $response->getHeader('Content-type');
if (is_array($ctype)) $ctype = $ctype[0];

$token = $response->getBody();
if ($ctype == 'text/html' || $ctype == 'text/xml') {
  $token = htmlentities($token);
}
//$token = 'QenUKS8bJ2Eg67u/kI2+HwGtr1ckJUDfcmBWaLdDNefb0JmMBp/27Wn1B8THIHLsZNgqhDrbKHp3ZDJ4Tnw64d0pWimgZ/PJ9DzSbqo8/4DbfTXWuOr57bAcahK/csrD8lUBwcrfgO48EYjryBKH6iV5tiDI3KaWqQIEjwsbt8Kcw3/AcV9wR01gfeJ4/fR2qBthdEHeIZK6Ekq+uW+6aCQK1s9/F09jOGNPKuxu9zaIZXMD85bxmbIqe9UBkHaSKIefc25v7cYp2PiZIPPFLfvpunVqWxwfJQb29DsgGrUrNkC9LEo1aRIyUvNPlNTgi7lknnq+1ecfcshF3KoXVJX+IY5U0sUgHJDrdwcuVOjDhKvXLqFjRVfFjqIBWfyH7aitzOGExtkjyQzLyi/t6UAzm+GFalOt9M590VZ4hr8QITbfJugs3AN+vuPgL2BID2nP41crdKvnVc3z4kIkWqz7+a+DdZF/IQ5jUlAp5xnLgdCytFWVUyS/5TPdkm8XjpeOyQIdlCygHzthFqN/IpoJ6e9rsvni1LBIJ7lK3DdTEptaS5xQvNth549Z+ubnyZ2P9iujWG+/ZRQCstvb46Mc+Agv0izfd9SlZLw10r1kVGBATH4IpaVe3JMR3bX7z5UVhcbxJ5/45UKAfa+B3tuMjy+Wj5+KjVzL6CZKfiu+ReNpx6eBUYfYha9yj/xZgAWQ0wTcx9FcnM6cyz816XDJ9lvWm5zm6mD9K8i0OsGhgwJsqNY+EgRVzGj3sltvjl1KBsRf+0M/G9iNuIHT8ZsUG0rRg8cXWeZLx4E3KvlzXMnm2FOfpYM5alyk+psEeyVWYCfCq4Jo5VPpl4YqQ06szmetvZfXMIlYVWtqpKXkduU2XgRrIOnQkIl/Ry98OQqI9Xo2PQlbVYfc8BzuaDNbGDHAj7LkRR6Ol7JqiMLq4dPRO33dJT4bfEbIhJjjutMd0xwZIZ7vhV53kgEej0A/YUsZc1mBO+tKXkS+NQ7sp9q6HINcvmUXxisQowjw6jGoCyi2Ruo5/4vApZk+fppmn9gyEiP63ZLLNPUvAbyEi1xCNN2f/CrlhLtgVjd+j2DipalhY7SARegGE1BZf6T8leK15AVn/nnOWJsoBL2QqpNOLtOFgmPylLubvwWp9CK7FFlMdEYTzYzhSmxGZNQ+FZ/sGrMVP36oWLV4fVM+Jl6GiV7IKnpPlKIxktW5SZyRjmpQQqQGbAyIUd+lGrbdZMg7Ucq44cetnl87jtHBZZvSVeSIQjscyQB95rs87+UWudf4iVwa7uPr2flWCQ==';

$transcriptId = 'c08f73f4-3c87-405f-8c7d-a18e0150151e';

$url           = 'http://cornerstone.cyberu-api.com/service.svc/transcript/'.$transcriptId;
$method        = 'GET';
$date          = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16
$signature     = GetSignature($method, $url, -1, $date);
$authorization = GetAuthHeader($signature,$date);

$client = new Zend_Http_Client();
$client->setUri($url);
$client->setConfig(array
  (
    'maxredirects' => 0,
    'timeout' => 30,
    'useragent' => 'CSOD_Sandbox',
    'keepalive' => true
  )
);

$client->setHeaders(array(
    'Authorization' => $authorization,
    'x-cyberu-auth' => $token,
  )
);
$client->setMethod(Zend_Http_Client::GET);
$response = $client->request();

$domRQ = new DOMDocument();
$domRQ->loadXML($response->getBody());
$domRQ->formatOutput = true;
header("Content-type: text/xml");
echo $domRQ->saveXML();
