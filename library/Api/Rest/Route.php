<?php
class Api_Rest_Route extends Zend_Rest_Route {

    public function match($path, $partial = false) { 

        $uri = $path->getRequestUri();
        $parts = explode('/',$uri);
        if ($parts['1'] == 'api' && isset($parts['2'])) {
            if(preg_match('/v([0-9]+[.])+([0-9]+)/',$parts['2'])) {
            	$version = $parts['2'];
                unset($parts['2']);
                $parts = array_values($parts);

                $front = Zend_Controller_Front::getInstance();
                $front->setControllerDirectory(
                        array(
                            'default' => APPLICATION_PATH.'/controllers',
                            'api' => APPLICATION_PATH.'/modules/api/controllers/'.$version
                            )
                        ,'api');
            }

            //here we add support for /rest/2.0/orders/apikey/12345
            //and  /rest/orders/apikey/12345
            //print_r($parts);
            if (isset($parts[3])) {
                $cnt = 0;
                $newparts = array();
                if (!is_numeric($parts[3])) {
                    while($cnt < 3) {
                        $newparts[] = array_shift($parts);
                        $cnt++;
                    }
                } else {//we maintain support for 'id'
                    while($cnt < 4) {
                        $newparts[] = array_shift($parts);
                        $cnt++;
                    }
                }
                $uri = implode('/',$newparts);
                $additional = $parts;
            } else {
                $uri = implode('/',$parts);
                $additional = false;
            }

            $path->setRequestUri($uri);
            $path->setPathInfo($uri);
        }
        
        $result = parent::match($path, $partial);
		//after we let Zend_Rest_Route do its job, we set the additional
        //parameters like apikey detected above
        if (isset($additional) && $additional && is_array($additional) && count($additional) > 0 ) {
            $key = true;
            for($i = 0; $i < count($additional); $i++) {
                if($i%2 == 0) {
                	if( isset($additional[$i +1]) && !empty($additional[$i]) ){
                    	$result[$additional[$i]] = $additional[$i + 1];
                	}
                }
            }
        }

        return $result;
    }
}