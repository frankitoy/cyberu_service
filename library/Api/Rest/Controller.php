<?php

class Api_Rest_Controller extends Zend_Rest_Controller 
{

	protected $_access_account = NULL;
    protected $_cache = false;
    private $_output = false;
    
	
    /**
     * abstract class
     */
    public function headAction()
    {
    }
    
    public function init() 
    {
    	if (Zend_Controller_Action_HelperBroker::hasHelper('layout')) 
        {
            $this->_helper->layout->disableLayout();
        }
        $this->_helper->viewRenderer->setNoRender(true);

        //set the response code
        $this->_response->setHttpResponseCode(501);

        //create an instance of our response object
        $this->_output = new Api_Rest_Response();
        //pass the Accept headers
        $this->_output->setAcceptheaders($this->_request->getHeader('Accept'));
        //set code for the output/response object
        $this->_output->setCode(501);
        //set the body of the output/response object
        $this->_output->setBody('Method Not Implemented');
        if( $this->_request->isGet() ){//do not validate if index view
            
        }else{
        
	        if ( $this->_request->apikey ){
				$controller = $this->_request->getControllerName();
	            $action = $this->_request->getActionName();
	            if ( $this->_hasAccess($this->_request->apikey) == false ) {//validate invalid string
	            	$this->_output->setCode(403);
	                $this->_output->setBody('Access denied');
	                header("HTTP/1.0 403 Access denied");
	                header('content-type: '.$this->_output->getType());
	                echo $this->_output->getOutput();
	                exit();
	            }
	        } else {
	            $this->_output->setCode(403);
	            $this->_output->setBody('Access denied');
	            header("HTTP/1.0 403 Access denied");
	            header('content-type: '.$this->_output->getType());
	            echo $this->_output->getOutput();
	            exit();
	        }
        }
    }

    public function indexAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function getAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function postAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function putAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function deleteAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    protected function _createResponse($code,$body,$type = 'auto') {
        $this->_response->setHttpResponseCode($code);
        $this->_output->setCode($code);
        $this->_output->setBody($body);

        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody(stripslashes($this->_output->getOutput($type)));
    }

    protected function _hasAccess($apikey) {
		if (Zend_Registry::isRegistered('Api_Access')) {
			$access = Zend_Registry::get('Api_Access');
			$apikey = sanitize_sql_string($apikey);//security purposes owasp.org
            $accessAccount = $access->hasApiAccessByApiKey($apikey);
            if (empty($accessAccount)) {
                return false;
            }
            $this->_access_account = $accessAccount;
            return true;
        }
		return false;
	}
}