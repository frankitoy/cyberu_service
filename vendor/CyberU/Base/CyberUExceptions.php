<?php
/**
 * Created by IntelliJ IDEA.
 * User: fmarcelo
 * Date: 2/15/13
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 *
 * CyberU Error
 *
 */
namespace CyberU\Base;

/*
 *
 * CyberU Exceptions
 *
 */
class CyberUExceptions extends Exception
{
  protected $result;

  /**
   * @param array $result
   * @param int $code
   * @param Exception $previous
   */
  public function __construct($result = array(), $code = 0, Exception $previous = null)
  {
    $this->result = $result;

    $code = isset($result['error_code']) ? $result['error_code'] : 0;

    if (isset($result['error_description'])) {
      $msg = $result['error_description'];
    } elseif (isset($result['error']) && is_array($result['error'])) {
      $msg = $result['error']['message'];
    } elseif (isset($result['error_msg'])) {
      // Rest server style
      $msg = $result['error_msg'];
    } else {
      $msg = 'Unknown Error. Check getResult()';
    }

    parent::__construct($msg, $code);
  }

  /**
   * Return the associated result object returned by the API server.
   *
   * @return array The result from the API server
   */
  public function getResult()
  {
    return $this->result;
  }

  /**
   * @param $method
   * @param array $args
   * @return Exception|null
   */
  public function __call($method, array $args)
  {
    if ('getprevious' == strtolower($method)) {
      return $this->_getPrevious();
    }

    return null;
  }

  /**
   * String representation of the exception
   *
   * @return string
   */
  public function __toString()
  {
    if (version_compare(PHP_VERSION, '5.3.0', '<')) {
      if (null !== ($e = $this->getPrevious())) {
        return $e->__toString()
          . "\n\nNext "
          . parent::__toString();
      }
    }

    return parent::__toString();
  }

  /**
   * Returns previous Exception
   *
   * @return Exception|null
   */
  protected function _getPrevious()
  {
    return $this->_previous;
  }
}
