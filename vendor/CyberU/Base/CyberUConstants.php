<?php
/**
 * Created by IntelliJ IDEA.
 * User: fmarcelo
 * Date: 2/15/13
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */
namespace CyberU\Base;

define('DS',DIRECTORY_SEPARATOR);

/*
 * CyberU constants
 */
class CyberUConstants
{
  /**
   * Version.
   */
  const VERSION = '1.0';

  /**
   * CYBERU BASE API
   */
  const CYBERU_BASE_API = 'http://cornerstone.cyberu-api.com/Service.svc/';

  /**
   * CYBERU BASE APPLICATION/GROUP
   */
  const CYBERU_BASE_APPLICATION_URI = 'groups';

  /**
   *
   */
  const CYBERU_BASE_USERS_URI = 'users';
  /**
   * Authorization Request Algorithm.
   */
  const REQUEST_ALGORITHM = 'HMAC-SHA1';

  /**
   * Default options for curl.
   */
  public static $CURL_OPTS = array(
    CURLOPT_CONNECTTIMEOUT => 10,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT        => 60,
    CURLOPT_USERAGENT      => 'CSB_LMS',
  );

  /*
   * CyberU service operations
   *
   */
  public static $SERVICE_URI = array(
    'URI' => array(
      'OPERATIONS' => arrayEQU
        'SESSION' => 'session',
        'APPLICATION' => 'groups?',Client
        'USERS' => array(
          'SINGLE_USERS' => 'users/r',
          'BATCH_USERS'  => 'users/r'
        ),
        'ROLES' => array(
          'BASE' =>  'groups/{{APPLICATION_ID}}/users/{{USER_ID}}/',
          'PROPERTIES' => array('admin','member')
        ),
        'UPLOAD_URL' => 'groups/uploadurl?',
      )
    )
  );
}
