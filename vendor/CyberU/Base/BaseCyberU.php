<?php
/**
 * Created by IntelliJ IDEA.
 * User: fmarcelo
 * Date: 2/15/13
 * Time: 1:00 PM
 * To change this template use File | Settings | File Templates.
 */
namespace CyberU\Base;
/**
 * abstract class BaseCyberU
 *
 * @property mixed DOMAIN_MAP
 */
use CyberUException;

abstract class BaseCyberU
{
  /**
   * Application Key.
   *
   * @var string
   */
  protected $applionKey;

  /**
   * Application Secret Key.
   *
   * @var string
   */
  protected $secretKey;

  /**
   * Application ID.
   *
   * @var string
   */
  protected $applicationId;

  /**
   * Client Id.
   *
   * @var string
   */
  protected $clientId;

  /**
   * Global UserId.
   *
   * @var string
   */
  protected $globalUserId;

  /**
   * Date Request.
   *
   * @var string
   */
  protected $dateRequest;

  /**
   * The data from the authorization_header token.
   * @var string
   */
  protected $authorizationHeader;

  /**
   * The data from the authorized_request token.
   * @var string
   */
  protected $authorizedRequest;

  /**
   * A Cross Site Request Forgery state variable to assist in the defense against CSRF attacks.
   * @var string
   */
  protected $state;

  protected $accessSignature = null;

  /**
   * A session token variable to store when accessing CyberU Api
   * @var string
   */
  protected $sessionToken = null;

  /**
   * Initialize CyberU Session Token.
   *
   * The configuration:
   * - applicationSecret: the application Secret
   * - secretKey: secret Key
   * - clientId: client Id
   * - globalUserId: global user Id
   * - applicationId: applicationId if exists
   *
   * @param array $config configuration
   */
  public function __construct($config)
  {
    $this->setApplicationSecret($config['applicationSecret']);
    $this->setSecretKey($config['secretKey']);
    $this->setClientId($config['clientId']);
    $this->setGlobalUserId($config['globalUserId']);
    $this->setDateRequest();
    if (isset($config['applicationId'])) {
      $this->setApplicationIdSupport($config['applicationId']);
    }

    if (isset($config['trustForwarded']) && $config['trustForwarded']) {
      $this->trustForwarded = true;
    }

    $state = $this->getPersistentData('state');
    if (!empty($state)) {
      $this->state = $state;
    }
  }

  /**
   * Set the Application Secret.
   *
   * @param string $applicationSecret Set Application Secret
   * @return $this
   */
  public function setApplicationSecret($applicationSecret)
  {
    $this->applicationSecret = $applicationSecret;

    return $this;
  }

  /**
etS  * Set the Secret Key.
   *
   * @param string $secretKey Set Secret Key.
   * @return $this
   */
  public function setSecretKey($secretKey)
  {
    $this->secretKey = $secretKey;

    return $this;
  }

  /**
   * Set the Client Id.
   *
   * @param string $clientId Set Client Id.
   * @return $this
   */
  public function setClientId($clientId)
  {
    $this->clientId = $clientId;

    return $this;
  }

  /**
   * Set the Global User Id.
   *
   * @param string $globalUserId Set Global User Id.
   * @return $this
   */
  public function setGlobalUserId($globalUserId)
  {
    $this->globalUserId = $globalUserId;

    return $this;
  }

  /**
   * Set the Date Request.
   * Time format YYYY-mm-ddThh:ii:ss 2013-02-13T08:12:12
   * @return $this
   */
  public function setDateRequest()
  {
    $this->dateRequest = date("Y-m-d")."T".date("H:i:s");

    return $this;
  }

  /**
   * Sets the access signature this input will be rendered as HTTP Authorization custom header
   *
   * @param $authorization_header
   * @internal param string $access_signature an access token.
   * @return $this
   */
  public function setAuthorizationHeader($authorization_header)
  {
    $this->authorizationHeader = $authorization_header;

    return $this;
  }

  /**
   * Sets the session token for api calls.
   *
   * @param $session_token
   * @internal param string $access_signature an access token.
   * @return $this
   */
  public function setSessionToken($session_token)
  {
    $this->sessionToken = $session_token;

    return $this;
  }

  /**
   * Set the Application Id.
   *
   * @param string $applicationId Set Application Id if exists.
   * @return $this
   */
  public function setApplicationIdSupport($applicationId)
  {
    $this->applicationId = $applicationId;

    return $this;
  }

  /**
   * Get the Application Secret.
   *
   * @return string the Application Secret
   */
  public function getApplicationSecret()
  {
    return $this->applicationSecret;
  }

  /**
   * Get the Secret Key.
   *
   * @return string the Secret Key
   */
  public function getSecretKey()
  {
    return $this->secretKey;
  }

  /**
   * Get the Client Id.
   *
   * @return string the Client Id
   */
  public function getClientId()
  {
    return $this->clientId;
  }

  /**
   * Get the Secret Key.
   *
   * @return string the Application Secret
   */
  public function getGlobalUserId()
  {
    return $this->globalUserId;
  }

  /**
   * Get the Application Id if exists in config
   *
   * @return string applicationId
   */
  public function getApplicationIdSupport()
  {
    return $this->applicationId;
  }

  /**
   * Get the date request .
   *
   * @return date timestamp.
   */
  public function getDateRequest()
  {
    return $this->dateRequest;
  }

  /**
   * Determines the session token that should be used for succeeding API calls for CyberU.
   * CyberU requires a custom HTTP Header called "Authorization". This authentication process
   * is pseudo implementation of oAuth.
   *
   * @param string $method. This parameter defines if the HTTP request is GET, POST, PUT or DELETE.
   * @param string $url     This parameter is the urldecoded link to service
   * @param string contentLength. This parameter should pass -1 if GET, 0 if POST data is empty,
   * http_build_query(array('session_token'=>'44d76ec9-2672-43ef-a290-a15e002de3e3'))
   * needs to be done before we do a strlen(POST).
   * @return string The session token
   *
   */
  public function getAccessToken($method,$url,$contentLength)
  {
    if ($this->sessionToken !== null) {
      return $this->sessionToken;
    }

    $this->setAuthorizationHeader($this->getAuthorizationHeader($method,$url,$contentLength));
    $session_token = $this->getCyberUSessionToken();
    if ($session_token) {
      $this->setSessionToken($session_token);
    }

    return $this->sessionToken;
  }

  /**
   * Determines the access signature that should be used for API calls.
   *
   * @param string $method. This parameter defines if the HTTP request is GET, POST, PUT or DELETE.
   * @param string $url     This parameter is the url decoded link to service
   * @param string contentLength. This parameter should pass -1 if GET, 0 if POST data is empty, http_build_query(array('session_token'=>'44d76ec9-2672-43ef-a290-a15e002de3e3'))
   * needs to be done before we do a strlen(POST).
   * @return string The session token
   *
   */
  private function getAuthorizationHeader($method='GET',$url=null,$contentLength=-1)
  {
    $sign = $method ."\n".strtolower($url)."\n" . $this->getApplicationSecret()."\n".$this->getDateRequest()."\n".$contentLength;
    $hmac = base64_encode(hash_hmac("sha1",utf8_encode($sign),utf8_encode($this->getSecretKey()),true));

    return strtolower(urlencode($hmac));//ogadiuyutgeygvsjrasirgdu3ne%3d
  }

  protected function getCyberUSessionToken()
  {
    // first, consider a signed request if it's supplied.
    // if there is a signed request, then it alone determines
    // the access token.
    $signed_request = $this->getAuthorizedRequest();
    /*
    if ($signed_request) {
      // apps.facebook.com hands the access_token in the signed_request
      if (array_key_exists('oauth_token', $signed_request)) {
        $access_token = $signed_request['oauth_token'];
        $this->setPersistentData('access_token', $access_token);

        return $access_token;
      }

      // the JS SDK puts a code in with the redirect_uri of ''
      if (array_key_exists('code', $signed_request)) {
        $code = $signed_request['code'];
        if ($code && $code == $this->getPersistentData('code')) {
          // short-circuit if the code we have is the same as the one presented
          return $this->getPersistentData('access_token');
        }

        $access_token = $this->getAccessTokenFromCode($code, '');
        if ($access_token) {
          $this->setPersistentData('code', $code);
          $this->setPersistentData('access_token', $access_token);

          return $access_token;
        }
      }

      // signed request states there's no access token, so anything
      // stored should be cleared.
      $this->clearAllPersistentData();

      return false; // respect the signed request's data, even
                    // if there's an authorization code or something else
    }

    $code = $this->getCode();
    if ($code && $code != $this->getPersistentData('code')) {
      $access_token = $this->getAccessTokenFromCode($code);
      if ($access_token) {
        $this->setPersistentData('code', $code);
        $this->setPersistentData('access_token', $access_token);

        return $access_token;
      }

      // code was bogus, so everything based on it should be invalidated.
      $this->clearAllPersistentData();

      return false;
    }

    // as a fallback, just return whatever is in the persistent
    // store, knowing nothing explicit (signed request, authorization
    // code, etc.) was present to shadow it (or we saw a code in $_REQUEST,
    // but it's the same as what's in the persistent store)*/
    return $this->getPersistentData('session_token');
  }

  /**
   * Retrieve the signed request, either from a request parameter or,
   * if not present, from a cookie.
   *
   * @return string the signed request, if available, or null otherwise.
   */
  public function getAuthorizedRequest()
  {
    if (!$this->authorizedRequest) {
      if (!empty($_REQUEST['authorized_request'])) {
        $this->authorizedRequest = $this->parseAuthorizedRequest($_REQUEST['authorized_request']);
      } elseif (!empty($_COOKIE[$this->getAuthorizedRequestCookieName()])) {
        $this->authorizedRequest = $this->parseAuthorizedRequest($_COOKIE[$this->getAuthorizedRequestCookieName()]);
      }
    }

    return $this->authorizedRequest;
  }

  /**
   * Get the UID of the connected user, or 0
   * if the Facebook user is not connected.
   *
   * @return string the UID if available.
   */
  public function getUser()
  {
    if ($this->user !== null) {
      // we've already determined this and cached the value.
      return $this->user;
    }

    return $this->user = $this->getUserFromAvailableData();
  }


  /**
   * Build the URL for api given parameters.
   *
   * @param $method String the method name.
   * @return string The URL for the given parameters
   */
  protected function getCyberUApiUrl($method)
  {
    $name = 'api';
    if (isset($READ_ONLY_CALLS[strtolower($method)])) {
      $name = 'api_read';
    } elseif (strtolower($method) == 'video.upload') {
      $name = 'api_video';
    }

    return self::getUrl($name, 'restserver.php');
  }

  /**
   * Build the URL for given domain alias, path and parameters.
   *
   * @param $name string The name of the domain
   * @param $path string Optional path (without a leading slash)
   * @param $params array Optional query parameters
   *
   * @return string The URL for the given parameters
   */
  protected function getUrl($name, $path='', $params=array())
  {
    $url = self::$DOMAIN_MAP[$name];
    if ($path) {
      if ($path[0] === '/') {
        $path = substr($path, 1);
      }
      $url .= $path;
    }
    if ($params) {
      $url .= '?' . http_build_query($params, null, '&');
    }

    return $url;
  }

  /**
   * Determines the connected user by first examining any signed
   * requests, then considering an authorization code, and then
   * falling back to any persistent store storing the user.
   *
   * @return integer The id of the connected Facebook user,
   *                 or 0 if no such user exists.
   */
  protected function getUserFromAvailableData()
  {
    // if a signed request is supplied, then it solely determines
    // who the user is.
    $signed_request = $this->getSignedRequest();
    if ($signed_request) {
      if (array_key_exists('user_id', $signed_request)) {
        $user = $signed_request['user_id'];

        if ($user != $this->getPersistentData('user_id')) {
          $this->clearAllPersistentData();
        }

        $this->setPersistentData('user_id', $signed_request['user_id']);

        return $user;
      }

      // if the signed request didn't present a user id, then invalidate
      // all entries in any persistent store.
      $this->clearAllPersistentData();

      return 0;
    }
    /*
    $user = $this->getPersistentData('user_id', $default = 0);
    $persisted_access_token = $this->getPersistentData('access_token');

    // use access_token to fetch user id if we have a user access_token, or if
    // the cached access token has changed.
    //$access_token = $this->getAccessToken();
    if ($access_token &&  $access_token != $this->getApplicationAccessToken() &&
        !($user && $persisted_access_token == $access_token)) {
      $user = $this->getUserFromAccessToken();
      if ($user) {
        $this->setPersistentData('user_id', $user);
      } else {
        $this->clearAllPersistentData();
      }
    }*/

    //return $user;
  }

  /**
   * Make an API call.
   *
   * @return mixed The decoded response
   */
  public function api(/* polymorphic */)
  {
    $args = func_get_args();
    if (is_array($args[0])) {
      return $this->_restserver($args[0]);
    } else {
      return call_user_func_array(array($this, '_graph'), $args);
    }
  }

  /**
   * Constructs and returns the name of the cookie that
   * potentially houses the signed request for the app user.
   * The cookie is not set by the BaseFacebook class, but
   * it may be set by the JavaScript SDK.
   *
   * @return string the name of the cookie that would house
   *         the signed request value.
   */
  protected function getAuthorizedRequestCookieName()
  {
    return 'cyberu_ar_'.$this->getApplicationSecret();
  }

  protected function getMetadataCookieName()
  {
    return 'cyberu_m_'.$this->getApplicationSecret();
  }

  /**
   * Get the authorization code from the query parameters, if it exists,
   * and otherwise return false to signal no authorization code was
   * discoverable.
   *
   * @return mixed The authorization code, or false if the authorization
   *               code could not be determined.
   */
  protected function getCode()
  {
    if (isset($_REQUEST['code'])) {
      if ($this->state !== null &&
        isset($_REQUEST['state']) &&
        $this->state === $_REQUEST['state']) {

        // CSRF state has done its job, so clear it
        $this->state = null;
        $this->clearPersistentData('state');

        return $_REQUEST['code'];
      } else {
        self::errorLog('CSRF state token does not match one provided.');

        return false;
      }
    }

    return false;
  }

  /**
   * Lays down a CSRF state token for this process.
   *
   * @return void
   */
  protected function establishCSRFTokenState()
  {
    if ($this->state === null) {
      $this->state = md5(uniqid(mt_rand(), true));
      $this->setPersistentData('state', $this->state);
    }
  }

  /**
   * Invoke the old restserver.php endpoint.
   *
   * @param array $params Method call object
   *
   * @return mixed The decoded response object
   * @throws FacebookApiException
   */
  protected function _restserver($params)
  {
    /*
    // generic application level parameters
    $params['api_key'] = $this->getAppId();
    $params['format'] = 'json-strings';

    $result = json_decode($this->_oauthRequest(
      $this->getApiUrl($params['method']),
      $params
    ), true);

    // results are returned, errors are thrown
    if (is_array($result) && isset($result['error_code'])) {
      $this->throwAPIException($result);
      // @codeCoverageIgnoreStart
    }
    // @codeCoverageIgnoreEnd

    $method = strtolower($params['method']);
    if ($method === 'auth.expiresession' ||
        $method === 'auth.revokeauthorization') {
      $this->destroySession();
    }

    return $result;*/
  }

  /**
   * Make a OAuth Request.
   *
   * @param string $url The path (required)
   * @param array $params The query/post data
   *
   * @return string The decoded response object
   * @throws FacebookApiException
   */
  protected function _oauthRequest($url, $params)
  {
    if (!isset($params['access_token'])) {
      //$params['access_token'] = $this->getAccessToken();
    }

    // json_encode all params values that are not strings
    foreach ($params as $key => $value) {
      if (!is_string($value)) {
        $params[$key] = json_encode($value);
      }
    }

    return $this->makeRequest($url, $params);
  }

  /**
   * Makes an HTTP request. This method can be overridden by subclasses if
   * developers want to do fancier things or use something other than curl to
   * make the request.
   *
   * @param string      $url    The URL to make the request to
   * @param array       $params The parameters to use for the POST body
   * @param CurlHandler $ch     Initialized curl handle
   *
   * @throws FacebookApiException
   * @return string               The response text
   */
  protected function makeRequest($url, $params, $ch=null)
  {
    if (!$ch) {
      $ch = curl_init();
    }

    $opts = self::$CURL_OPTS;
    if ($this->getFileUploadSupport()) {
      $opts[CURLOPT_POSTFIELDS] = $params;
    } else {
      $opts[CURLOPT_POSTFIELDS] = http_build_query($params, null, '&');
    }
    $opts[CURLOPT_URL] = $url;

    // disable the 'Expect: 100-continue' behaviour. This causes CURL to wait
    // for 2 seconds if the server does not support this header.
    if (isset($opts[CURLOPT_HTTPHEADER])) {
      $existing_headers = $opts[CURLOPT_HTTPHEADER];
      $existing_headers[] = 'Expect:';
      $opts[CURLOPT_HTTPHEADER] = $existing_headers;
    } else {
      $opts[CURLOPT_HTTPHEADER] = array('Expect:');
    }

    curl_setopt_array($ch, $opts);
    $result = curl_exec($ch);

    if (curl_errno($ch) == 60) { // CURLE_SSL_CACERT
      self::errorLog('Invalid or no certificate authority found, '.
        'using bundled information');
      curl_setopt($ch, CURLOPT_CAINFO,
        dirname(__FILE__) . '/fb_ca_chain_bundle.crt');
      $result = curl_exec($ch);
    }

    // With dual stacked DNS responses, it's possible for a server to
    // have IPv6 enabled but not have IPv6 connectivity.  If this is
    // the case, curl will try IPv4 first and if that fails, then it will
    // fall back to IPv6 and the error EHOSTUNREACH is returned by the
    // operating system.
    if ($result === false && empty($opts[CURLOPT_IPRESOLVE])) {
      $matches = array();
      $regex = '/Failed to connect to ([^:].*): Network is unreachable/';
      if (preg_match($regex, curl_error($ch), $matches)) {
        if (strlen(@inet_pton($matches[1])) === 16) {
          self::errorLog('Invalid IPv6 configuration on server, '.
            'Please disable or get native IPv6 on your server.');
          self::$CURL_OPTS[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;
          curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
          $result = curl_exec($ch);
        }
      }
    }

    if ($result === false) {
      $e = new FacebookApiException(array(
        'error_code' => curl_errno($ch),
        'error' => array(
          'message' => curl_error($ch),
          'type' => 'CurlException',
        ),
      ));
      curl_close($ch);
      throw $e;
    }
    curl_close($ch);

    return $result;
  }

  /**
   * Parses a signed_request and validates the signature.
   *
   * @param $authorized_request
   * @internal param string $signed_request A signed token
   * @return array The payload inside it or null if the sig is wrong
   */
  protected function parseAuthorizedRequest($authorized_request)
  {
    list($encoded_sig, $payload) = explode('.', $authorized_request, 2);

    // decode the data
    $sig = self::base64UrlDecode($encoded_sig);
    $data = json_decode(self::base64UrlDecode($payload), true);

    if (strtoupper($data['algorithm']) !== self::REQUEST_ALGORITHM) {
      self::errorLog('Unknown algorithm. Expected ' . self::REQUEST_ALGORITHM);

      return null;
    }

    // check sig
    $expected_sig = hash_hmac('sha1', $payload,
      $this->getApplicationSecret(), $raw = true);
    if ($sig !== $expected_sig) {
      self::errorLog('Bad Signed JSON signature!');

      return null;
    }

    return $data;
  }

  /**
   * Makes a signed_request blob using the given data.
   *
   * @param array The data array.
   * @return string The signed request.
   */
  protected function makeSignedRequest($data)
  {
    if (!is_array($data)) {
      throw new InvalidArgumentException(
        'makeSignedRequest expects an array. Got: ' . print_r($data, true));
    }
    $data['algorithm'] = self::SIGNED_REQUEST_ALGORITHM;
    $data['issued_at'] = time();
    $json = json_encode($data);
    $b64 = self::base64UrlEncode($json);
    $raw_sig = hash_hmac('sha256', $b64, $this->getAppSecret(), $raw = true);
    $sig = self::base64UrlEncode($raw_sig);

    return $sig.'.'.$b64;
  }


  /**
   * Build the URL for given domain alias, path and parameters.
   *
   * @internal param string $name The name of the domain
   * @internal param string $path Optional path (without a leading slash)
   * @internal param array $params Optional query parameters
   *
   * @return string The URL for the given parameters
   */

  /*
  protected function getUrl($name, $path='', $params=array())
  {
    $url = self::$DOMAIN_MAP[$name];
    if ($path) {
      if ($path[0] === '/') {
        $path = substr($path, 1);
      }
      $url .= $path;
    }
    if ($params) {
      $url .= '?' . http_build_query($params, null, '&');
    }

    return $url;
  }*/

  protected function getHttpHost()
  {
    if ($this->trustForwarded && isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
      return $_SERVER['HTTP_X_FORWARDED_HOST'];
    }

    return $_SERVER['HTTP_HOST'];
  }

  protected function getHttpProtocol()
  {
    if ($this->trustForwarded && isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
      if ($_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
        return 'https';
      }

      return 'http';
    }
    /*apache + variants specific way of checking for https*/
    if (isset($_SERVER['HTTPS']) &&
      ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] == 1)) {
      return 'https';
    }
    /*nginx way of checking for https*/
    if (isset($_SERVER['SERVER_PORT']) &&
      ($_SERVER['SERVER_PORT'] === '443')) {
      return 'https';
    }

    return 'http';
  }

  /**
   * Get the base domain used for the cookie.
   */
  protected function getBaseDomain()
  {
    // The base domain is stored in the metadata cookie if not we fallback
    // to the current hostname
    $metadata = $this->getMetadataCookie();
    if (array_key_exists('base_domain', $metadata) &&
      !empty($metadata['base_domain'])) {
      return trim($metadata['base_domain'], '.');
    }

    return $this->getHttpHost();
  }

  /**
   * Returns the Current URL, stripping it of known FB parameters that should
   * not persist.
   *
   * @return string The current URL
   */
  protected function getCurrentUrl()
  {
    $protocol = $this->getHttpProtocol() . '://';
    $host = $this->getHttpHost();
    $currentUrl = $protocol.$host.$_SERVER['REQUEST_URI'];
    $parts = parse_url($currentUrl);

    $query = '';
    if (!empty($parts['query'])) {
      // drop known fb params
      $params = explode('&', $parts['query']);
      $retained_params = array();
      foreach ($params as $param) {
        if ($this->shouldRetainParam($param)) {
          $retained_params[] = $param;
        }
      }

      if (!empty($retained_params)) {
        $query = '?'.implode($retained_params, '&');
      }
    }

    // use port if non default
    $port =
      isset($parts['port']) &&
        (($protocol === 'http://' && $parts['port'] !== 80) ||
          ($protocol === 'https://' && $parts['port'] !== 443))
        ? ':' . $parts['port'] : '';

    // rebuild
    return $protocol . $parts['host'] . $port . $parts['path'] . $query;
  }

  /**
   * Returns true if and only if the key or key/value pair should
   * be retained as part of the query string.  This amounts to
   * a brute-force search of the very small list of Facebook-specific
   * params that should be stripped out.
   *
   * @param string $param A key or key/value pair within a URL's query (e.g.
   *                     'foo=a', 'foo=', or 'foo'.
   *
   * @return boolean
   */
  protected function shouldRetainParam($param)
  {
    foreach (self::$DROP_QUERY_PARAMS as $drop_query_param) {
      if (strpos($param, $drop_query_param.'=') === 0) {
        return false;
      }
    }

    return true;
  }

  /**
   * Analyzes the supplied result to see if it was thrown
   * because the access token is no longer valid.  If that is
   * the case, then we destroy the session.
   *
   * @param $result array A record storing the error message returned
   *                      by a failed API call.
   * @throws CyberUException
   */
  protected function throwAPIException($result)
  {
    $e = new CyberUException($result);
    switch ($e->getType()) {

      // REST server errors are just Exceptions
      case 'Exception':
        $message = $e->getMessage();
        $this->destroySession();
        break;
    }
    throw $e;
  }

  /**
   * Prints to the error log if you aren't in command line mode.
   *
   * @param string $msg Log message
   */
  protected static function errorLog($msg)
  {
    // disable error log if we are running in a CLI environment
    // @codeCoverageIgnoreStart
    if (php_sapi_name() != 'cli') {
      error_log($msg);
    }
    // uncomment this if you want to see the errors on the page
    // print 'error_log: '.$msg."\n";
    // @codeCoverageIgnoreEnd
  }

  /**
   * Base64 encoding that doesn't need to be urlencode()ed.
   * Exactly the same as base64_encode except it uses
   *   - instead of +
   *   _ instead of /
   *   No padded =
   *
   * @param string $input base64UrlEncoded string
   * @return string
   */
  protected static function base64UrlDecode($input)
  {
    return base64_decode(strtr($input, '-_', '+/'));
  }

  /**
   * Base64 encoding that doesn't need to be urlencode()ed.
   * Exactly the same as base64_encode except it uses
   *   - instead of +
   *   _ instead of /
   *
   * @param string $input string
   * @return string base64Url encoded string
   */
  protected static function base64UrlEncode($input)
  {
    $str = strtr(base64_encode($input), '+/', '-_');
    $str = str_replace('=', '', $str);

    return $str;
  }

  /**
   * Destroy the current session
   */
  public function destroySession()
  {
    $this->accessToken = null;
    $this->signedRequest = null;
    $this->user = null;
    $this->clearAllPersistentData();

    // Javascript sets a cookie that will be used in getSignedRequest that we
    // need to clear if we can
    $cookie_name = $this->getSignedRequestCookieName();
    if (array_key_exists($cookie_name, $_COOKIE)) {
      unset($_COOKIE[$cookie_name]);
      if (!headers_sent()) {
        $base_domain = $this->getBaseDomain();
        setcookie($cookie_name, '', 1, '/', '.'.$base_domain);
      } else {
        // @codeCoverageIgnoreStart
        self::errorLog(
          'There exists a cookie that we wanted to clear that we couldn\'t '.
            'clear because headers was already sent. Make sure to do the first '.
            'API call before outputing anything.'
        );
        // @codeCoverageIgnoreEnd
      }
    }
  }

  /**
   * Parses the metadata cookie that our Javascript API set
   *
   * @return  an array mapping key to value
   */
  protected function getMetadataCookie()
  {
    $cookie_name = $this->getMetadataCookieName();
    if (!array_key_exists($cookie_name, $_COOKIE)) {
      return array();
    }

    // The cookie value can be wrapped in "-characters so remove them
    $cookie_value = trim($_COOKIE[$cookie_name], '"');

    if (empty($cookie_value)) {
      return array();
    }

    $parts = explode('&', $cookie_value);
    $metadata = array();
    foreach ($parts as $part) {
      $pair = explode('=', $part, 2);
      if (!empty($pair[0])) {
        $metadata[urldecode($pair[0])] =
          (count($pair) > 1) ? urldecode($pair[1]) : '';
      }
    }

    return $metadata;
  }

  protected static function isAllowedDomain($big, $small)
  {
    if ($big === $small) {
      return true;
    }

    return self::endsWith($big, '.'.$small);
  }

  protected static function endsWith($big, $small)
  {
    $len = strlen($small);
    if ($len === 0) {
      return true;
    }

    return substr($big, -$len) === $small;
  }

  /**
   * Persistent store, but another subclass--one that you implement--
   * might use a database, memcache, or an in-memory cache.
   *

   */

  /**
   * Stores the given ($key, $value) pair, so that future calls to
   * getPersistentData($key) return $value. This call may be in another request.
   *
   * @param string $key
   * @param array $value
   *
   * @return void
   */
  abstract protected function setPersistentData($key, $value);

  /**
   * @param string $key The key of the data to retrieve
   * @param boolean $default The default value to return if $key is not found
   *
   * @return mixed
   */
  abstract protected function getPersistentData($key, $default = false);

  /**
   * Clear the data with $key from the persistent storage
   *
   * @param string $key
   * @return void
   */
  abstract protected function clearPersistentData($key);

  /**
   * Clear all data from the persistent storage
   *
   * @return void
   */
  abstract protected function clearAllPersistentData();
}
