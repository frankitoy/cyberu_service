<?php
namespace CyberU\Base;
/**
 * Created by IntelliJ IDEA.
 * User: fmarcelo
 * Date: 2/15/13
 * Time: 2:01 PM
 * To change this template use File | Settings | File Templates.
 */
//if (!function_exists('curl_init')) {
  throw new Exception('CyberU needs the CURL PHP extension.');
//}

if (!function_exists('json_decode')) {
  throw new Exception('CyberU needs the JSON PHP extension.');
}

if (!class_exists('SimpleXMLElement')) {
  throw new Exception('CyberU needs the SimpleXMLElement PHP extension');
}

class CyberU extends BaseCyberU
{
  const CYBERU_COOKIE_NAME = 'CyberU';

  // We can set this to a high number because the main session
  // expiration will trump this.
  const CYBERU_COOKIE_EXPIRE = 31556926; // 1 year

  // Stores the shared session ID if one is set.
  protected $sharedSessionID;

  public function __construct($config)
  {
    if (!session_id()) {
      session_start();
    }
    parent::__construct($config);
  }

  protected static $supportedKeys = array('session_token');

  /**
   * Provides the implementations of the inherited abstract
   * methods.  The implementation uses PHP sessions to maintain
   * a store for authorization codes, CSRF states, and
   * access tokens.
   */
  protected function setPersistentData($key, $value)
  {
    if (!in_array($key, self::$supportedKeys)) {
      self::errorLog('Unsupported key passed to setPersistentData.');

      return;
    }

    $session_var_name = $this->constructSessionVariableName($key);
    $_SESSION[$session_var_name] = $value;
  }

  protected function getPersistentData($key, $default = false)
  {
    if (!in_array($key, self::$supportedKeys)) {
      self::errorLog('Unsupported key passed to getPersistentData.');

      return $default;
    }

    $session_var_name = $this->constructSessionVariableName($key);

    return isset($_SESSION[$session_var_name]) ?
      $_SESSION[$session_var_name] : $default;
  }

  protected function clearPersistentData($key)
  {
    if (!in_array($key, self::$supportedKeys)) {
      self::errorLog('Unsupported key passed to clearPersistentData.');

      return;
    }

    $session_var_name = $this->constructSessionVariableName($key);
    unset($_SESSION[$session_var_name]);
  }

  protected function clearAllPersistentData()
  {
    foreach (self::$supportedKeys as $key) {
      $this->clearPersistentData($key);
    }
    if ($this->sharedSessionID) {
      $this->deleteSharedSessionCookie();
    }
  }

  protected function deleteSharedSessionCookie()
  {
    $cookie_name = $this->getSharedSessionCookieName();
    unset($_COOKIE[$cookie_name]);
    $base_domain = $this->getBaseDomain();
    setcookie($cookie_name, '', 1, '/', '.'.$base_domain);
  }

  protected function getSharedSessionCookieName()
  {
    return self::CYBERU_COOKIE_NAME . '_' . $this->getApplicationSecret();
  }

  protected function constructSessionVariableName($key)
  {
    $parts = array($this->getApplicationSecret(), $key);
    if ($this->sharedSessionID) {
      array_unshift($parts, $this->sharedSessionID);
    }

    return implode('_', $parts);
  }
}
