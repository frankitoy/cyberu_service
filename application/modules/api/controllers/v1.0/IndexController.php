<?php

use CyberU\Authentication\Config;

class Api_IndexController extends Api_Rest_Controller 
{
    private function validateAccessKeyAgainstInfoId()
    {//validate the info id with the access key
        return ( $this->_request->info == true) ? true: false;
    }
    
    private function createAccessDeniedResponse()
    {
        $this->_createResponse(403,'Access denied');
    }

    public function indexAction() 
    {
        Zend_Loader::loadClass('Zend_Http_Client');   
        
        

        $config = new CyberU\Authentication\Config();
        $method = 'GET';
        $date   = date("Y-m-d")."T".date("H:i:s");//2013-01-30T21:30:16    
        $url    = 'http://cornerstone.cyberu-api.com/service.svc/session/' .$GlobalUserId;
        echo $url;
        $this->_createResponse(200,'ok');


        /*
        $signature = GetSignature($method, $url, -1, $date);
        $authorization = GetAuthHeader($signature,$date);

        $client = new Zend_Http_Client();
        $client->setUri($url);
        $client->setConfig(array
          (
              'maxredirects' => 0,
              'timeout' => 30,
              'useragent' => 'CSOD_Sandbox',
              'keepalive' => true
          )
        );
        $client->setHeaders('Authorization',$authorization);
        $client->setMethod(Zend_Http_Client::GET);
        $response = $client->request();

        $ctype = $response->getHeader('Content-type');
        if (is_array($ctype)) $ctype = $ctype[0];

        $token = $response->getBody();
        if ($ctype == 'text/html' || $ctype == 'text/xml') {
            $token = htmlentities($token);
        } */
    }

    public function postAction(){//this is the same as MediaController uploadMediaItem line 9 of Mike Kerr
        $this->createAccessDeniedResponse();
    }
}