<?php

class Api_UsersController extends Api_Rest_Controller {
    
    private function validateAccessKeyAgainstInfoId(){//validate the info id with the access key
        return ( $this->_request->info == true) ? true: false;
    }
    
    private function createAccessDeniedResponse(){
        $this->_createResponse(403,'Access denied');
    }

    public function getAction()
    {
        echo 'went get';
    }
    
    public function indexAction() 
    {
        echo 'went index';
    }

    public function postAction(){//this is the same as MediaController uploadMediaItem line 9 of Mike Kerr
        $this->createAccessDeniedResponse();
    }
}