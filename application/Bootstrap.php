<?php

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require dirname(__DIR__) .DIRECTORY_SEPARATOR. 'library'.DIRECTORY_SEPARATOR.$fileName;
    //require dirname(__DIR__) .DIRECTORY_SEPARATOR. 'library'.DIRECTORY_SEPARATOR.'CyberU'.DIRECTORY_SEPARATOR.'Authentication'.DIRECTORY_SEPARATOR.$fileName;
}

spl_autoload_register('autoload');


class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	private $version = 'v1.0';
	
	protected function _initAttackFiltering(){
		Zend_Loader::loadFile('Sanitize/sanitize.php');
		Zend_Loader::loadFile('UUID/UUID.php');
	}
	
	protected function _initDoctype()
	{
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('HTML5');
	}

	protected function _initAppAutoload() {
	
		$baseURI  = $_SERVER['HTTP_HOST'];
		$protocol = 'http';
		if( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ){
			$protocol = 'https';
		}
		
		$baseURI  = $protocol .'://'.$baseURI;
		Zend_Registry::set('baseUri',$baseURI);
		
		$autoloader = new Zend_Application_Module_Autoloader(array(
			'namespace' => 'Api',
			'basePath' => APPLICATION_PATH,
		));
	}
	
	protected function _initRestroutes() 
	{
		$this->bootstrap('frontController');
		$frontController = Zend_Controller_Front::getInstance();
		$router = $frontController->getRouter();
	
		$restRoute = new Api_Rest_Route($frontController,array(),array('api'));
		$router->addRoute('api', $restRoute);
		$router->addRoute('v1',new Zend_Controller_Router_Route('api/'.$this->version,
                array(
                	'controller' => 'index',
                    'action' => 'index'
                )
			)
		);

		$router->addRoute('v1',new Zend_Controller_Router_Route('api/'.$this->version.'/application',
                array(
                	'controller' => 'index',
                    'action' => 'application'
                )
			)
		);

		$router->addRoute('v1',new Zend_Controller_Router_Route('api/'.$this->version.'/admin',
                array(
                	'controller' => 'index',
                    'action' => 'application'
                )
			)
		);

		$router->addRoute('v1',new Zend_Controller_Router_Route('api/'.$this->version.'/users',
                array(
                	'controller' => 'index',
                    'action' => 'application'
                )
			)
		);
	}
	
	private function getDb(){
		$this->bootstrap('db');
		return $this->getResource('db');
	}
	
	protected function _initZFDebug()
    {
    	$this->bootstrap('db');
    	$db =  $this->getPluginResource('db')->getDbAdapter();
    	$autoloader = Zend_Loader_Autoloader::getInstance();
	    $autoloader->registerNamespace('ZFDebug');
	
	    $options = array(
	        'plugins' => array('Variables', 
	                           'Database' => array('adapter' => $db), 
	                           'Memory', 
	                           'Time', 
	                           'Registry', 
	                           'Exception')
	    );
	    $debug = new ZFDebug_Controller_Plugin_Debug($options);
	
	    $this->bootstrap('frontController');
	    $frontController = $this->getResource('frontController');
	    $frontController->registerPlugin($debug);
	}
}
